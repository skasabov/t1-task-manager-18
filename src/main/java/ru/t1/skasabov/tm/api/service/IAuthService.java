package ru.t1.skasabov.tm.api.service;

import ru.t1.skasabov.tm.model.User;

public interface IAuthService {

    User registry(String login, String password, String email);

    void login(String login, String password);

    void logout();

    Boolean isAuth();

    String getUserId();

    User getUser();

}

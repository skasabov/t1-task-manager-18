package ru.t1.skasabov.tm.command.user;

import ru.t1.skasabov.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    private static final String NAME = "view-user-profile";

    private static final String DESCRIPTION = "View profile of current user.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final User user = serviceLocator.getAuthService().getUser();
        System.out.println("[USER VIEW PROFILE]");
        showUser(user);
    }

}
